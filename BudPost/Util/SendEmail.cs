﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.WebPages.Html;
using WebMatrix.WebData;

namespace BudPost.Util
{
    public class SendEmail
    {
        public string EmailSetting(string subject = null, string body = null, string from = null, string emailTo = null)
        {
            string emailEnviado = emailTo;
            MailMessage objEmail = new MailMessage();
            //objEmail.From = new MailAddress("BudFreela<" + from + ">");
            objEmail.From = new MailAddress(from, "BudPost", System.Text.Encoding.UTF8);
            objEmail.To.Add(emailTo);
            objEmail.Priority = MailPriority.High;
            objEmail.IsBodyHtml = true;
            objEmail.Subject = subject;
            objEmail.Body = body;
            objEmail.SubjectEncoding = System.Text.Encoding.UTF8;
            objEmail.BodyEncoding = System.Text.Encoding.UTF8;
                        
            //Enviando E-Mail com autenticação:
            SmtpClient objSmtp = new SmtpClient();
            objSmtp.Credentials = new System.Net.NetworkCredential("budteste@gmail.com", "testando123");
            objSmtp.Host = "smtp.gmail.com";
            objSmtp.Port = 587;
            //Caso utilize conta de email do exchange deve habilitar o SSL
            objSmtp.EnableSsl = true;

            //Enviando E-Mail sem autenticação:
            //SmtpClient objSmtp = new SmtpClient();
            //objSmtp.Host = "smtp.gmail.com";
            //objSmtp.Port = 587;
            
            try
            {
                objSmtp.Send(objEmail);
                return "Um E-mail foi enviado para o endereço: " + emailEnviado;
            }
            catch (Exception)
            {
                return "Falha ao enviar E-Mail!";
            }
            finally
            {
                //excluímos o objeto de e-mail da memória
                objEmail.Dispose();
                //anexo.Dispose();
            }
        }
    }
}