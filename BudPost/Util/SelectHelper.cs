﻿using BudPost.Areas.Admin.Models;
using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.WebPages.Html;

namespace BudPost.Util
{
    public class SelectHelper
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        #region SelectList

        public List<SelectListItem> ListRoles()
        {
            SortedList<string, string> textRole = new SortedList<string, string>();
            textRole.Add("admin", "Administrador");
            textRole.Add("user", "Usuário");
            List<string> roles = Roles.GetAllRoles().ToList();
            List<SelectListItem> selectRoles = roles.Select(role => new SelectListItem() { Value = role.ToString(), Text = textRole[role].ToString() }).ToList();

            return selectRoles;
        }

        public List<SelectListItem> ListGender()
        {
            List<SelectListItem> gender = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text = "Masculino",
                    Value = "M"
                },
                new SelectListItem()
                {
                    Text = "Feminino",
                    Value = "F"
                }
            };
            return gender;
        }

        public List<SelectListItem> ListStatus()
        {
            List<SelectListItem> status = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text = "Solteiro(a)",
                    Value = "Solteiro(a)"
                },
                new SelectListItem()
                {
                    Text = "Casado(a)",
                    Value = "Casado(a)"
                },
                new SelectListItem()
                {
                    Text = "Viúvo(a)",
                    Value = "Viúvo(a)"
                },
                new SelectListItem()
                {
                    Text = "Divorciado(a)",
                    Value = "Divorciado(a)"
                }
            };
            return status;
        }

        public List<SelectListItem> ListTypePhones()
        {
            List<SelectListItem> type = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text = "Fixo",
                    Value = "Fixo"
                },
                new SelectListItem()
                {
                    Text = "Celular",
                    Value = "Celular"
                },
                new SelectListItem()
                {
                    Text = "Fax",
                    Value = "Fax"
                },
                new SelectListItem()
                {
                    Text = "Outros",
                    Value = "Outros"
                }
            };
            return type;
        }

        #endregion SelectList

        #region SelectList Database

        public List<SelectListItem> ListCountries()
        {
            List<SelectListItem> selectCountries = unitOfWork.CountryRepository.Countries.Select(country => new SelectListItem() { Value = country.CountryId.ToString(), Text = country.Name }).ToList();

            return selectCountries;
        }

        public List<SelectListItem> ListStates()
        {
            List<SelectListItem> selectStates = unitOfWork.StateRepository.States.Select(state => new SelectListItem() { Value = state.StateId.ToString(), Text = state.Name }).ToList();

            return selectStates;
        }        

        public List<SelectListItem> ListCities()
        {
            List<SelectListItem> selectCities = unitOfWork.CityRepository.Cities.Select(city => new SelectListItem() { Value = city.CityId.ToString(), Text = city.Name }).ToList();

            return selectCities;
        }

        #endregion SelectList Database
        
    }
}
