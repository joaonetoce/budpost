﻿using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace BudPost.Controllers
{
    [Authorize(Roles = "admin, user")]
    public class PosterController : Controller
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        #region View Actions

        // GET: Poster
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(Poster poster)
        {
            poster.DataUser_DataUserId = WebSecurity.CurrentUserId;
            //System.Diagnostics.Debug.WriteLine("UserId : " + poster.DataUser_DataUserId + " - Título: " + poster.Title + " - Descrição: " + poster.Description);
            if(ModelState.IsValid)
            {
                try
                {
                    unitOfWork.PosterRepository.CreatePost(poster);
                    unitOfWork.PosterRepository.Save();
                    TempData["MsgAlert"] = "Postado com sucesso!";
                    return RedirectToAction("Create");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("Description", "Erro ao postar!.");
                }                
            }
            TempData["MsgAlert"] = "Falhou!";
            return RedirectToAction("Create");
        }

        #endregion View Actions
    }
}