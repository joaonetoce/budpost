﻿using BudPost.Models;
using BudPost.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace BudPost.Controllers
{
    public class AccountController : Controller
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        #region View Actions

        //GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login loginData, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string username = unitOfWork.DataUserRepository.FindUsernameForEmail(loginData.Username);

                    if (WebSecurity.Login(username, loginData.Password))
                    {
                        if (Roles.IsUserInRole(username, "user"))
                        {
                            
                            if (ReturnUrl != null)
                            {
                                return Redirect(ReturnUrl);
                            } 
                             
                            return RedirectToAction("Index", "Home", new { area = "" });
                        }
                        if (Roles.IsUserInRole(username, "admin"))
                        {                             
                            return RedirectToAction("Index", "Home", new { area = "Admin" });
                        }
                        else 
                        {
                            ModelState.AddModelError("", "Este usuário está desativado.");
                            return View(loginData);
                        }
                        
                    }
                    else
                    {
                        ModelState.AddModelError("", "Usuário ou senha incorreta.");
                        return View(loginData);
                    }
                }
                catch(Exception)
                {
                    ModelState.AddModelError("", "Não foi possível realizar o login.");
                }
            }
	
            ModelState.AddModelError("", "Não foi possível realizar o login.");
            return View(loginData);
        }

        //GET: Register
        public ActionResult Register() 
        {            
           return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterDataUser registerDataUser)
        {
            string emailRegex = string.Format("{0}{1}", @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))", @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
            Regex rg = new Regex(emailRegex);

            if (unitOfWork.DataUserRepository.AllDataUsers.Where(d => d.Email.Equals(registerDataUser.DataUser.Email)).Count() > 0)
            {
                ModelState.AddModelError("Email", "Este E-mail já pertence a outro usuário.");
                return View(registerDataUser);
            }
            if (!rg.IsMatch(registerDataUser.DataUser.Email))
            {
                ModelState.AddModelError("Email", "o Email tem um formato inválido.");
                return View(registerDataUser);
            }
            else 
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateUserAndAccount(registerDataUser.Register.Username, registerDataUser.Register.Password);
                        Roles.AddUserToRole(registerDataUser.Register.Username, registerDataUser.Register.Role);

                        registerDataUser.DataUser.DataUserId = WebSecurity.GetUserId(registerDataUser.Register.Username);
                        unitOfWork.DataUserRepository.CreateDataUser(registerDataUser.DataUser);
                        unitOfWork.DataUserRepository.Save();

                        if (WebSecurity.UserExists(registerDataUser.Register.Username) && unitOfWork.DataUserRepository.FindDataUserForUserId(WebSecurity.GetUserId(registerDataUser.Register.Username)) == null)
                        {
                            Roles.RemoveUserFromRole(registerDataUser.Register.Username, Roles.GetRolesForUser()[0]);
                            ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(registerDataUser.Register.Username);
                            ((SimpleMembershipProvider)Membership.Provider).DeleteUser(registerDataUser.Register.Username, true);
                        }

                        ViewBag.msgAlert = "Seu usuário foi cadastrado com sucesso, bem vindo ao BudPost!";
                        return View("ComeBack");
                    }
                    catch (System.Web.Security.MembershipCreateUserException)
                    {
                        ModelState.AddModelError("", "Esse nome de usuário já está sendo utilizado.");
                        return View("Register", registerDataUser);
                    }
                    catch (System.ArgumentNullException)
                    {
                        ModelState.AddModelError("", "Esse campo não pode ser nulo.");
                        return View("Register", registerDataUser);
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException)
                    {
                        ModelState.AddModelError("", "Não foi possível gravar os dados de usuário.");
                        return View("Register", registerDataUser);
                    }                                     
                }               
            }

            if (WebSecurity.UserExists(registerDataUser.Register.Username) && unitOfWork.DataUserRepository.FindDataUserForUserId(WebSecurity.GetUserId(registerDataUser.Register.Username)) == null)
            {
                Roles.RemoveUserFromRole(registerDataUser.Register.Username, Roles.GetRolesForUser()[0]);
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(registerDataUser.Register.Username);
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(registerDataUser.Register.Username, true);
            }
            ModelState.AddModelError("", "Não foi possível registrar esse usuário.");
            return View("Register", registerDataUser);
        }

        //GET: Exclude
        public ActionResult Exclude()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ConfirmExclude()
        {
            try
            {
                SimpleMembershipProvider provider = (SimpleMembershipProvider)Membership.Provider;
                string username = User.Identity.Name;
                long id = provider.GetUserId(username);

                Roles.RemoveUserFromRole(username, Roles.GetRolesForUser()[0]);
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(username);
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser((username), true);               
                
                unitOfWork.DataUserRepository.Delete(id);
                unitOfWork.DataUserRepository.Save();
                
                Session.RemoveAll();
                Session.Abandon();
                WebSecurity.Logout();
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Não foi possível excluir este usuário, tente novamente mais tarde!");
                return View("Exclude");
            }
            ViewBag.msgAlert = "Volte sempre!";
            return View("ComeBack");
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePassword changePassword)
        {
            if (ModelState.IsValid)
            {
                if (WebSecurity.ChangePassword(User.Identity.Name, changePassword.Password, changePassword.NewPassword))
                {
                    ViewBag.alterOk = "Alterado com sucesso!";
                    return View(changePassword);
                }
                else
                {
                    ModelState.AddModelError("", "Não foi possível alterar a senha.");
                    return View(changePassword);
                }
            }
            return View(changePassword);
        }

        [Authorize(Roles = "user, admin")]
        public ActionResult Logout()
        {          
            Session.RemoveAll();
            Session.Abandon();
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ComeBack()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult LostPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LostPassword(LostPassword model)
        {
            //MembershipUser user;
            if (ModelState.IsValid)
            {
                long idCurrent = WebSecurity.GetUserId(model.Username);

                if (WebSecurity.UserExists(model.Username))
                {
                    try
                    {
                        var token = WebSecurity.GeneratePasswordResetToken(model.Username);
                        // Generate the html link sent via email
                        string resetLink = "<a href='" + Url.Action("ResetPassword", "Account", new { rt = token }, "http") + "'>Reset Password Link</a>";
                        // Email stuff
                        string subject = "Resetar a sua senha no site BudFPost.com.br";
                        string body = "Clique no link para resetar a sua senha: " + resetLink;
                        string from = "budfreela@budweb.com.br";
                        string emailTo = model.Username;

                        SendEmail email = new SendEmail();
                        string msg = email.EmailSetting(subject, body, from, emailTo);                        
                        ViewBag.msgAlert = msg;
                        return View("ComeBack");
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "Falha ao emviar o E-Mail.");
                    }
                }
                else // Email not found
                {
                    ModelState.AddModelError("", "Esse e-mail não está cadastrado no sistema." + model.Username);
                }
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string rt)
        {
            ResetPassword model = new ResetPassword();
            model.ReturnToken = rt;

            return View(model);
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPassword model)
        {
            if (ModelState.IsValid)
            {
                bool resetResponse = WebSecurity.ResetPassword(model.ReturnToken, model.Password);
                if (resetResponse)
                {
                    ViewBag.msgAlert = "Senha alterada com sucesso!";
                    return View("Login");
                }
                else
                {
                    ModelState.AddModelError("", "Não foi possível alterar a senha.");
                    return View(model);
                }
            }
                return View(model);
        }

        #endregion View Actions
    }    
}