﻿using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BudPost.Controllers
{
    [Authorize(Roles = "admin, user")]
    public class HomeController : Controller
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        #region View Actions

        public ActionResult Index()
        {
            return View(unitOfWork.PosterRepository.Posters.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Settings()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public JsonResult GetPoster()
        {
            List<Poster> posters = unitOfWork.PosterRepository.Posters.ToList();

            return Json(posters, JsonRequestBehavior.AllowGet);
        }

        #endregion View Actions
    }
}