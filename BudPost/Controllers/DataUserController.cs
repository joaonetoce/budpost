﻿using BudPost.Areas.Admin.Models;
using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace BudPost.Controllers
{
    [Authorize(Roles = "admin, user")]
    public class DataUserController : Controller
    {

        private UnitOfWork unitOfWork = new UnitOfWork();

        #region View Actions

        // GET: DataUser
        public ActionResult EditUser()
        {
            long id = WebSecurity.GetUserId(User.Identity.Name);
            DataUser dataUser = unitOfWork.DataUserRepository.MyDataUser();
            
            EditDataUser editDataUser = new EditDataUser();
            editDataUser.DataUser = dataUser;
            editDataUser.Phones = unitOfWork.PhoneRepository.FindPhoneForUser(id);
            
            if (dataUser == null)
            {
                return HttpNotFound();
            }

            return View(editDataUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(EditDataUser editDataUser, IEnumerable<Phone> phones)
        {
            
            if (ModelState.IsValid && editDataUser.DataUser.DataUserId != 0)
            {
                if (editDataUser.phone != null)
                {
                    try
                    {
                        editDataUser.phone.DataUser_DataUserId = WebSecurity.CurrentUserId;
                        unitOfWork.PhoneRepository.CreatePhone(editDataUser.phone);
                    }
                    catch
                    {
                        ModelState.AddModelError("", "ERRO ao criar um novo telefone!");
                    }
                }
                if(phones != null)
                {
                    try
                    {
                        foreach (var phone in phones)
                        {
                            //System.Diagnostics.Debug.WriteLine("PhoneId: " + phone.PhoneId + " - Type: " + phone.Type + " - Number: " + phone.Number + " - DataUser_DataUserId: " + phone.DataUser_DataUserId);
                            unitOfWork.PhoneRepository.UpdatePhone(phone);
                        }                        

                        foreach (var p in phones.Where(p => p.Number == null && p.DataUser_DataUserId == WebSecurity.CurrentUserId))
                        {
                            //System.Diagnostics.Debug.WriteLine("PhoneId: " + p.PhoneId + " - Type: " + p.Type + " - Number: " + p.Number + " - DataUser_DataUserId: " + p.DataUser_DataUserId);
                            unitOfWork.PhoneRepository.Delete(p.PhoneId);
                        }

                        unitOfWork.PhoneRepository.Save();
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", "ERRO ao atualizar os dados do telefone!");
                    }
                }                

                try
                {                    
                    unitOfWork.DataUserRepository.UpdateDataUser(editDataUser.DataUser);
                    editDataUser.Phones = unitOfWork.PhoneRepository.FindPhoneForUser(editDataUser.DataUser.DataUserId);
                    unitOfWork.DataUserRepository.Save();
                    ViewBag.msgAlert = "Dados atualizados com sucesso!";
                    return View(editDataUser);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Não foi possível atualizar os dados desse usuário! ERRO: " + e.ToString());
                    return View(editDataUser);
                }
            }            
            return View(editDataUser);
        }

        public ActionResult AddPhone(long idUser)
        {
            if (idUser != 0)
            {
                try
                {
                    Phone phone = new Phone();
                    phone.DataUser_DataUserId = idUser;
                    unitOfWork.PhoneRepository.CreatePhone(phone);
                    unitOfWork.PhoneRepository.Save();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Erro!");
                }                                
            }

            DataUser dataUser = unitOfWork.DataUserRepository.MyDataUser();
            EditDataUser editDataUser = new EditDataUser();
            editDataUser.DataUser = dataUser;
            editDataUser.Phones = unitOfWork.PhoneRepository.FindPhoneForUser(idUser);

            return View("EditUser", editDataUser);
        }

        public ActionResult RemovePhone(long id)
        {
            if (id != 0)
            {
                try
                {
                    unitOfWork.PhoneRepository.Delete(id);
                    unitOfWork.PhoneRepository.Save();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Erro!");
                }
            }

            DataUser dataUser = unitOfWork.DataUserRepository.MyDataUser();
            EditDataUser editDataUser = new EditDataUser();
            editDataUser.DataUser = dataUser;
            long idUser = WebSecurity.GetUserId(User.Identity.Name);
            editDataUser.Phones = unitOfWork.PhoneRepository.FindPhoneForUser(idUser);

            return View("EditUser", editDataUser);
        }

        [HttpPost]
        public ActionResult SavePhone(IEnumerable<Phone> phones)
        {
            try
            {
                foreach (var phone in phones)
                {
                    //System.Diagnostics.Debug.WriteLine("PhoneId: " + phone.PhoneId + " - Type: " + phone.Type + " - Number: " + phone.Number + " - DataUser_DataUserId: " + phone.DataUser_DataUserId);
                    unitOfWork.PhoneRepository.UpdatePhone(phone);
                }

                foreach (var p in phones.Where(p => p.Number == null && p.DataUser_DataUserId == WebSecurity.CurrentUserId))
                {
                    //System.Diagnostics.Debug.WriteLine("PhoneId: " + p.PhoneId + " - Type: " + p.Type + " - Number: " + p.Number + " - DataUser_DataUserId: " + p.DataUser_DataUserId);
                    unitOfWork.PhoneRepository.Delete(p.PhoneId);
                }

                unitOfWork.PhoneRepository.Save();
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "ERRO!");
            }

            long id = WebSecurity.GetUserId(User.Identity.Name);
            DataUser dataUser = unitOfWork.DataUserRepository.MyDataUser();

            EditDataUser editDataUser = new EditDataUser();
            editDataUser.DataUser = dataUser;
            editDataUser.Phones = unitOfWork.PhoneRepository.FindPhoneForUser(id);

            return View("EditUser", editDataUser);
        }

        #endregion View Actions

        #region Json Actions

        [HttpGet]
        public JsonResult GetCountries()
        {
            List<Country> countries = unitOfWork.CountryRepository.Countries.ToList();

            return Json(countries, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetStates(long? countryId)
        {
            List<State> states = unitOfWork.StateRepository.States.Where(s => s.Country_Id.Equals(countryId)).ToList();

            return Json(states, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCities(long? stateId)
        {
            //System.Diagnostics.Debug.WriteLine("ID : " + stateId);
            List<City> cities = unitOfWork.CityRepository.Cities.Where(s => s.State_Id.Equals(stateId)).ToList();

            return Json(cities, JsonRequestBehavior.AllowGet);
        }        

        #endregion Json Actions
    }
}