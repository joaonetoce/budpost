﻿$(document).ready(function () {

    createMask();
    AppMaskPhone();
});

function createMask() {

    $(".mask-date").mask("99/99/9999");
    $(".mask-cpf").mask("999.999.999-99");
    $(".mask-pj").mask("99.999.999.9999-99");
    $(".mask-cep").mask("99-999-999");
    $(".mask-mobile").mask("(99)9.9999-9999");
    $(".mask-fix").mask("(99)9999-9999");
}

function AppMaskPhone(){
    $(".selectPhoneType").change(function(){
        var e = $(this).find('option:selected').attr('value');       
        
        if(e == "Celular"){
            $(".inputPhoneNumber").removeClass("active-input-location").unmask().mask("(99)9.9999-9999");
        }        
        else {
            $(".inputPhoneNumber").removeClass("active-input-location").unmask().mask("(99)9999-9999");
            if (e == "") {
                $(".inputPhoneNumber").addClass("active-input-location").unmask().val(null);
            }
        }
    });
}

function AddOrRemovePhone() {
    console.log("Entrou!");
    if ($(".enterAddPhone").hasClass("hidden")) {
        $(".enterAddPhone").removeClass("hidden");
    }
    else {
        $(".enterAddPhone").addClass("hidden").val(null);
    }
}

//----------------------------- Carregamento dos inputs de localização--------------------------------------// 

function LoadCountries() {
    $(".selectCountry").html("");
    
    $.getJSON("/DataUser/GetCountries", null, function (data) {
        $(".selectCountry").append("<option value=''>Selecione um país</option>");
        $.each(data, function (Index, country) {
            $(".selectCountry").append("<option value='" + country.CountryId + "'>" + country.Name + "</option>");
        });
    });    
}

    function LoadStates() {
        $(".selectState").removeClass("active-input-location");
        var countryId = $(".selectCountry").val();
        var stateId = $(".selectState").val();

        $(".selectState").html("");
        var paramsState = {};
        paramsState.countryId = countryId;

        $.getJSON("/DataUser/GetStates", paramsState, function (data) {
            $(".selectState").append("<option value=''>Selecione um estado</option>");
            $.each(data, function (Index, state) {
                $(".selectState").append("<option value='" + state.StateId + "'>" + state.Name + "</option>");
            });
        });
    }

    function LoadCities() {
        $(".selectCity").removeClass("active-input-location");
        var stateId = $(".selectState").val();
    
        $(".selectCity").html("");

        var paramsCity = {};
        paramsCity.stateId = stateId;

        $.getJSON("/DataUser/GetCities", paramsCity, function (data) {
            $(".selectCity").append("<option value=''>Selecione uma cidade</option>");
            $.each(data, function (Index, city) {
                $(".selectCity").append("<option value='" + city.CityId + "'>" + city.Name + "</option>");
            });
        });
    }

    function LoadResetCityInput(){
        var stateId = $(".selectState").val();
        $(".selectCity").html("");
        var paramsCity = {};
        paramsCity.stateId = stateId;

        $.getJSON("/DataUser/GetCities", paramsCity, function (data) {
            $(".selectCity").append("<option value=''>Selecione uma cidade</option>");
            $.each(data, function (Index, city) {
                $(".selectCity").append("<option value='" + city.CityId + "'>" + city.Name + "</option>");
            });
        });
    }

    //-------------------------------------------------------------------------------------------------------------//
