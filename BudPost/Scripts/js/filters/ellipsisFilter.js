﻿angular.module('budPost').filter("ellipsis", function () {
    return function (input, size) {        
        if (input.length <= size) return input;
        var output = input.substring(0, (size || 100)) + "...";
        console.log(output);
        return output;
    };
});