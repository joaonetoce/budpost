﻿angular.module('budPost').filter("nameDefault", function () {
    return function (input) {
        var titles = input.split(" ");
        var titleFormat = titles.map(function (title) {
            if(/(da|de)/.test(title)) return title;
            return title.charAt(0).toUpperCase() + title.substring(1).toLowerCase();
        });
        //console.log(titleFormat);
        return titleFormat.join(" ");
    };
});