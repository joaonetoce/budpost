﻿angular.module('budPost').service("posterAPI", function ($http, config) {
    this.getPosters = function () {
        return $http.get(config.baseHomeUrl + "GetPoster");
    };
});

/*
angular.module('budPost').factory("posterAPI", function ($http) {
    var _getPosters = function () {
        return $http.get("/Home/GetPoster");
    };

    //var _savePosters = function () {
        //return $http.post("/Home/GetPoster", poster);
    //};

    return {
        getPosters: _getPosters//,
        //savePosters: _savePosters
    };
});
*/