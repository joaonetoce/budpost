﻿angular.module('budPost').controller('homeCtrl', function ($scope, $filter, posterAPI) {
    //$scope.posters = [];

    var loadPosters = function () {
        posterAPI.getPosters().success(function (data, status) {
            //sconsole.log(data);
            $scope.posters = data;
        }).error(function (data, status) {
            $scope.message = "Aconteceu um problema na exibição dos POSTS! ERRO: " + data;
        });
    };

//LOAD MÉTODOS

    loadPosters();

});