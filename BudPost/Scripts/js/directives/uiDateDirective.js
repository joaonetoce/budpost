﻿angular.module("budPost").directive("uiDate", function () {
    return {
        require: "ngModel", 
        link: function (scope, element, attrs, ctrl) {
            var _formatDate = function (date) {
                date = date.replace(/[^0-9]+/g, "");
                if (date.length > 2) {
                    date = date.substring(0, 2) + "/" + date.substring(2);
                }
                if (date.length > 5) {
                    date = date.substring(0, 5) + "/" + date.substring(5, 9);
                }
                return date;
            };

            element.bind("keyup", function () {
                //console.log(ctrl.$viewValue);
                ctrl.$setViewValue(_formatDate(ctrl.$viewValue));
                ctrl.$render();
            });

            /*
            //Tempo em milissegundos(UNIX)
            ctrl.$parsers.push(function (value) {
                //console.log(value);
                if (value.length === 10)
                {
                    var dateArray = value.split("/");
                    console.log(new Date(dateArray[2], dateArray[1] - 1, dateArray[0]).getTime());
                    return new Date(dateArray[2], dateArray[1]-1, dateArray[0]).getTime();
                }                
            });

            //Formatar data
            ctrl.$formatters.push(function (value) {
                return $filters("date")(value, "dd/MM/yyyy");
            });
            */
        }
    };
});