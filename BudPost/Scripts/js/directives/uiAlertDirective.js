﻿angular.module("budPost").directive("uiAlert", function () {
    return {
        templateUrl: "/Error/Alert",
        replace: true,
        restrict: "E", //"AE"
        scope: {
            topic: "@title", //Se 'topic' for igual a 'title' basta colocar @
            error: "=message"
        }
    };
});