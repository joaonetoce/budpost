﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BudPost.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class SettingController : Controller
    {

        #region View Actions

        // GET: Admin/Setting
        public ActionResult Index()
        {
            return View();
        }

        #endregion View Actions
    }
}