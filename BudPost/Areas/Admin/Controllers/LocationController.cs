﻿using BudPost.Areas.Admin.Models;
using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BudPost.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class LocationController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        #region View Actions

        // GET: Admin/Home
        public ActionResult Country()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Country(Country country)
        {
            if(unitOfWork.CountryRepository.Countries.Where(c=>c.Name.Equals(country.Name)).Count() > 0)
            {
                ModelState.AddModelError("Name", "Esse país já está cadastrado!");
                return View(country);
            }

            if (unitOfWork.CountryRepository.Countries.Where(c => c.Initials.Equals(country.Initials)).Count() > 0)
            {
                ModelState.AddModelError("Initials", "Essa sigla já está associada a outro país!");
                return View(country);
            }

            if(ModelState.IsValid)
            {
                try
                {
                    country.Initials.ToUpper();
                    unitOfWork.CountryRepository.CreateCountry(country);
                    unitOfWork.CountryRepository.Save();                  
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Erro ao adicionar país!");
                    return View(country);
                }
            }
            TempData["MsgAlert"] = "País gravado com sucesso!";
            return RedirectToAction("Country");
        }
        
        public ActionResult States()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult States(State state)
        {
            if (unitOfWork.StateRepository.States.Where(c => c.Name.Equals(state.Name)).Count() > 0)
            {
                ModelState.AddModelError("Name", "Esse estado já está cadastrado!");
                return View(state);
            }

            if (unitOfWork.StateRepository.States.Where(c => c.Acronym.Equals(state.Acronym)).Count() > 0)
            {
                ModelState.AddModelError("Acronym", "Essa sigla já está associada a outro estado!");
                return View(state);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    state.Acronym.ToUpper();
                    unitOfWork.StateRepository.CreateState(state);
                    unitOfWork.StateRepository.Save();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Erro ao adicionar estado!");
                    return View(state);
                }
            }
            TempData["MsgAlert"] = "Estado gravado com sucesso!";
            return RedirectToAction("States");
        }

        [Authorize(Roles = "admin")]
        public ActionResult City()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult City(City city)
        {
            if (unitOfWork.CityRepository.Cities.Where(c => c.Name.Equals(city.Name)).Count() > 0)
            {
                ModelState.AddModelError("Name", "Essa cidade já está cadastrada!");
                return View(city);
            }

            if(ModelState.IsValid)
            {
                try 
                {
                    unitOfWork.CityRepository.CreateCity(city);
                    unitOfWork.CityRepository.Save();
                }
                catch(Exception)
                {
                    ModelState.AddModelError("", "Erro ao adicionar estado!");
                    return View(city);
                }
            }
            TempData["MsgAlert"] = "Cidade gravado com sucesso!";
            return RedirectToAction("City");
        }
                
        public ActionResult AllLocation()
        {
            return View();
        }
                
        [HttpPost]
        public string AllLocation(SettingLocation setLocation)
        {
            return "País: " + setLocation.Country.CountryId + " - " + "Estado: " + setLocation.State.StateId + " - " + "Cidade: " + setLocation.City.CityId ;
        }

        #endregion View Actions

    }
}