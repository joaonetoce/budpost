﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudPost.Areas.Admin.Models
{
    public class City
    {
        [Key]
        public long CityId { get; set; }

        [DataType(DataType.Text)]
        [StringLength(25)]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Essa cidade precisa estar associado a um estado!")]
        [Column("State_Id")]
        public long State_Id { get; set; }
    }
}