﻿using BudPost.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudPost.Areas.Admin.Models
{
    public class Country
    {
        [Key]
        public long CountryId { get; set; }

        [DataType(DataType.Text)]
        [StringLength(20)]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [StringLength(10)]
        public string Initials { get; set; }

        [ForeignKey("Country_Id")]
        public ICollection<State> States { get; set; }
    }
}