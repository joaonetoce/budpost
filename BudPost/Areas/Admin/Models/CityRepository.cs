﻿using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Areas.Admin.Models
{

    public class CityRepository : IDisposable
    {
        private bool disposed = false;
        private BudPostContext context;

        public CityRepository(BudPostContext context)
        {
            this.context = context;
        }

        public void CreateCity(City city)
        {
            context.Cities.Add(city);
        }

        public List<City> Cities 
        {
            get
            {
                return context.Cities.ToList();
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}