﻿using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Areas.Admin.Models
{
    public class CountryRepository : IDisposable
    {
        private bool disposed = false;
        private BudPostContext context;

        public CountryRepository(BudPostContext context)
        {
            this.context = context;
        }

        //Métodos
        public void CreateCountry(Country country)
        {
            context.Countries.Add(country);
        }

        public List<Country> Countries
        {
            get 
            {
                return context.Countries.ToList();
            }
        }

        public Country FindCountryForUserId(long? id)
        {
            return context.Countries.Find(id);
        }

        public void Delete(int id)
        {
            Country country = FindCountryForUserId(id);
            context.Countries.Remove(country);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}