﻿using BudPost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Areas.Admin.Models
{
    public class StateRepository : IDisposable
    {
        private bool disposed = false;
        private BudPostContext context;

        public StateRepository(BudPostContext context)
        {
            this.context = context;
        }

        public void CreateState(State state)
        {
            context.States.Add(state);
        }

        public List<State> States
        {
            get 
            {
                return context.States.ToList();
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}