﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudPost.Areas.Admin.Models
{
    public class State
    {
        [Key]
        public long StateId { get; set; }

        [DataType(DataType.Text)]
        [StringLength(25)]
        public string Name { get; set; }

        [DataType(DataType.Text)]
        [StringLength(10)]
        public string Acronym { get; set; }
        
        [Required(ErrorMessage="Esse estado precisa estar associado a um país!")]
        [Column("Country_Id")]
        public long Country_Id { get; set; }

        [ForeignKey("State_Id")]
        public ICollection<City> Cities { get; set; }
    }
}