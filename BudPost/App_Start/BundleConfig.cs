﻿using System.Web;
using System.Web.Optimization;

namespace BudPost
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            
            //JQuery-UI
            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));
            
            //Mask
            bundles.Add(new ScriptBundle("~/bundles/mask").Include(
                        "~/Scripts/jquery.maskedinput.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //Materialize CSS
            bundles.Add(new StyleBundle("~/Content/materialize").Include(
                      "~/Content/materialize/css/materialize.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //Common
            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                        "~/Scripts/common.js"));

            //Materialize
            bundles.Add(new ScriptBundle("~/bundles/materialize-js").Include(
                        "~/Scripts/materialize/materialize.js"));
                        
            //AngularJS
            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/i18n/angular-locale_pt-br.js",
                        "~/Scripts/js/app.js",
                        "~/Scripts/js/value/configValue.js"));
            
            //AngularJS - Controllers
            bundles.Add(new ScriptBundle("~/bundles/angularCtrl").Include(
                        "~/Scripts/js/controllers/*Ctrl.js"));

            //AngularJS - Services
            bundles.Add(new ScriptBundle("~/bundles/angularService").Include(
                        "~/Scripts/js/services/*Service.js"));

            //AngularJS - Filters
            bundles.Add(new ScriptBundle("~/bundles/angularFilters").Include(
                        "~/Scripts/js/filters/*Filter.js"));

            //AngularJS - Directives
            bundles.Add(new ScriptBundle("~/bundles/angularDirectives").Include(
                        "~/Scripts/js/directives/*Directive.js"));
        }
    }
}
