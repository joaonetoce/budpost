﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudPost.Models
{

    #region ClassLogin

    public class Login
    {
        [Required(ErrorMessage = "Entre com o seu nome de usuário.")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Entre com a sua senha!")]
        public string Password { get; set; }
    }

    #endregion ClassLogin

    #region ClassRegister

    public class Register
    {
        [Required(ErrorMessage = "Cadastre um nome de usuário.")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Cadastre uma senha.")]
        public string Password { get; set; }       

        [Display(Name = "Confirmar Senha")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "A senha não foi confirmada corretamente.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Defina um tipo de usuário.")]
        public string Role { get; set; }
    }

    #endregion ClassRegister

    #region ClassChangeUsername

    public class ChangeUsername
    {
        [Required(ErrorMessage = "Escolha um nome de usuário.")]
        public string Username { get; set; }
    }

    #endregion ClassChangeUsername

    #region ClassChangePassword

    public class ChangePassword
    {
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Digite sua senha.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Digite a nova senha.")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirmar a nova senha")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "A senha não foi confirmada corretamente.")]
        public string ConfirmNewPassword { get; set; }
    }

    #endregion ClassChangePassword

    #region ClassLostPassword

    public class LostPassword
    {
        [Required(ErrorMessage = "Entre com o seu Nome de Usuário.")]
        public string Username { get; set; }
    }

    #endregion ClassLostPassword

    #region ClassResetPassword

    public class ResetPassword
    {
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Você precisa de uma Senha!")]
        public string Password { get; set; }

        [NotMapped]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "A senha não foi confirmada corretamente.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Antes é preciso confirmar o seu email!")]
        public string ReturnToken { get; set; }
    }

    #endregion ClassResetPassword
}