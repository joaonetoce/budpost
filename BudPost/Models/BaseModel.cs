﻿using BudPost.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    #region ClassRegisterDataUser

    public class RegisterDataUser
    {
        public Register Register { get; set; }
        public DataUser DataUser { get; set; }
    }

    #endregion ClassRegisterDataUser

    #region ClassSettingLocation

    public class SettingLocation
    {
        public Country Country { get; set; }
        public State State { get; set; }
        public City City { get; set; }
    }

    #endregion ClassSettingLocation

    #region ClassEditDataUser

    public class EditDataUser
    {        
        public DataUser DataUser { get; set; }

        public List<Phone> Phones { get; set; }

        public Phone phone { get; set; }
    }

    #endregion ClassEditDataUser
}