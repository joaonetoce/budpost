﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class PhoneRepository : IDisposable
    {
        private bool disposed = false;
        private BudPostContext context;

        #region ConstructorMethod

        public PhoneRepository(BudPostContext context)
        {
            this.context = context;
        }

        #endregion ConstructorMethod

        #region EntityMethods

        public void CreatePhone(Phone phone)
        {
            this.context.Phones.Add(phone);
        }

        public void UpdatePhone(Phone phone)
        {
            this.context.Entry(phone).State = EntityState.Modified;
        }

        public List<Phone> Phones
        {
            get 
            {
                return this.context.Phones.ToList();
            }
        }

        public List<Phone> FindPhoneForUser(long id)
        {
            return this.context.Phones.Where(u => u.DataUser_DataUserId == id).ToList();
        }

        public Phone FindPhoneForId(long id)
        {
            return this.context.Phones.Find(id);
        }

        public void Delete(long id)
        {
            Phone phone = this.context.Phones.Find(id);
            this.context.Phones.Remove(phone);
        }
        public void DeleteAllPhones(long id)
        {
            List<Phone> phones = this.context.Phones.Where(p => p.DataUser_DataUserId.Equals(id) && p.Number == null).ToList();
            this.context.Phones.RemoveRange(phones);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        #endregion EntityMethods

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Dispose
    }
}