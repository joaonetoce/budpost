﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class Repository<T> : IRepository<T>, IDisposable where T : class
    {
        private bool disposed = false;
        private BudPostContext context;

        public Repository(BudPostContext context)
        {
            this.context = context;
        }

        public IQueryable<T> GetAll()
        {
            return context.Set<T>();
        }

        public IQueryable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return context.Set<T>().Where(predicate);
        }

        public T Find(params object[] key)
        {
            return context.Set<T>().Find(key);
        }

        public T First(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return context.Set<T>().Where(predicate).FirstOrDefault();
        }

        public void Add(T entity)
        {
            context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public void Delete(Func<T, bool> predicate)
        {
            context.Set<T>().Where(predicate).ToList().ForEach(del => context.Set<T>().Remove(del));

        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}