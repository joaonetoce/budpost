﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class Phone
    {
        [Key]
        public long PhoneId { get; set; }

        [DataType(DataType.Text)]
        public string Type { get; set; }

        [DataType(DataType.Text)]
        public string Number { get; set; }

        public long? DataUser_DataUserId { get; set; }
    }
}