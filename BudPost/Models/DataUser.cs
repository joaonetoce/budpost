﻿using BudPost.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    [Table("DataUser")]
    public class DataUser
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public long DataUserId { get; set; }

        [Display(Name = "Nome")]
        [DataType(DataType.Text)]
        [StringLength(25, MinimumLength = 2)]
        [Required(ErrorMessage = "Você precisa de um nome!")]
        public string FirstName { get; set; }

        [Display(Name = "Sobrenome")]
        [DataType(DataType.Text)]
        [StringLength(25, MinimumLength = 2)]
        [Required(ErrorMessage = "Você precisa de um sobrenome!")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Entre com o seu E-Mail.")]
        public string Email { get; set; }

        
        [Display(Name = "Endereço")]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 5)]
        public string Address { get; set; }

        [Display(Name = "Número")]
        [StringLength(10, MinimumLength = 1)]
        [DataType(DataType.Text)]
        public string Number { get; set; }

        [Display(Name = "Complemento")]
        [StringLength(50, MinimumLength = 5)]
        [DataType(DataType.Text)]
        public string Complement { get; set; }

        [Display(Name = "Bairro")]
        [DataType(DataType.Text)]
        [StringLength(25, MinimumLength = 2)]
        public string District { get; set; }

        [StringLength(25, MinimumLength = 1)]
        [DataType(DataType.PostalCode)]
        public string CEP { get; set; }


        public long? Country_CountryId { get; set; }

        [ForeignKey("Country_CountryId")]
        public virtual Country Country { get; set; }

        public long? State_StateId { get; set; }

        [ForeignKey("State_StateId")]
        public virtual State State { get; set; }

        public long? City_CityId { get; set; }

        [ForeignKey("City_CityId")]
        public virtual City City { get; set; }

        public long? Phone_PhoneId { get; set; }

        [ForeignKey("Phone_PhoneId")]
        public virtual Phone Phone { get; set; }
        
        [Display(Name = "Data de Nascimento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[Required(ErrorMessage = "Qual a sua data de Nascimento?")]
        public DateTime? BirthDate { get; set; }
        
        [Display(Name = "Sexo")]
        [DataType(DataType.Text)]
        [StringLength(10, MinimumLength = 1)]
        public string Gender { get; set; }

        [Display(Name = "Estado Civil")]
        [DataType(DataType.Text)]
        [StringLength(25, MinimumLength = 1)]
        public string Status { get; set; }

        [Display(Name = "CPF")]
        public string CPF { get; set; }

        [Display(Name = "PJ")]
        public string PJ { get; set; }

        [ForeignKey("DataUser_DataUserId")]
        public ICollection<Phone> Phones { get; set; }
    } 
}