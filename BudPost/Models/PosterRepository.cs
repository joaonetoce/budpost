﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class PosterRepository
    {
        private bool disposed = false;
        private BudPostContext context;

        #region ConstructorMethod

        public PosterRepository(BudPostContext context)
        {
            this.context = context;
        }

        #endregion ConstructorMethod

        #region EntityMethods

        //Métodos
        public void CreatePost(Poster poster)
        {
            context.Posters.Add(poster);
        }

        public List<Poster> Posters
        {
            get
            {
                return context.Posters.ToList();
            }
        }

        public Poster FindPostForUserId(long? id)
        {
            return context.Posters.Find(id);
        }

        public void Delete(int id)
        {
            Poster poster = FindPostForUserId(id);
            context.Posters.Remove(poster);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        #endregion EntityMethods

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Dispose
    }
}