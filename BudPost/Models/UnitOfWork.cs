﻿using BudPost.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class UnitOfWork : IDisposable
    {
        private bool disposed = false;
        private BudPostContext context = new BudPostContext();

        #region VarPrivateRepository

        private Repository<Object> repository;
        private DataUserRepository dataUserRepository;
        private PhoneRepository phoneRepository;
        private PosterRepository posterRepository;

        //Admin
        private CountryRepository countryRepository;
        private StateRepository stateRepository;
        private CityRepository cityRepository;

        #endregion VarPrivateRepository

        #region Methods

        public Repository<Object> Repository
        {
            get
            {
                if (repository == null)
                {
                    repository = new Repository<Object>(context);
                }
                return repository;
            }
        }

        public DataUserRepository DataUserRepository
        {
            get
            {
                if (dataUserRepository == null)
                {
                    dataUserRepository = new DataUserRepository(context);
                }
                return dataUserRepository;
            }
        }

        public PhoneRepository PhoneRepository
        {
            get
            {
                if (phoneRepository == null)
                {
                    phoneRepository = new PhoneRepository(context);
                }
                return phoneRepository;
            }
        }

        public PosterRepository PosterRepository
        {
            get
            {
                if (posterRepository == null)
                {
                    posterRepository = new PosterRepository(context);
                }
                return posterRepository;
            }
        }

        //Admin
        public CountryRepository CountryRepository
        {
            get
            {
                if (countryRepository == null)
                {
                    countryRepository = new CountryRepository(context);
                }
                return countryRepository;
            }
        }

        public StateRepository StateRepository
        {
            get
            {
                if (stateRepository == null)
                {
                    stateRepository = new StateRepository(context);
                }
                return stateRepository;
            }
        }

        public CityRepository CityRepository
        {
            get
            {
                if (cityRepository == null)
                {
                    cityRepository = new CityRepository(context);
                }
                return cityRepository;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Methods
    }
}