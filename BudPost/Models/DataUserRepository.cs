﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.WebPages.Html;
using WebMatrix.WebData;

namespace BudPost.Models
{
    public class DataUserRepository : IDisposable
    {
        private bool disposed = false;
        private BudPostContext context;

        #region ConstructorMethod

        public DataUserRepository(BudPostContext context)
        {
            this.context = context;
        }

        #endregion ConstructorMethod

        #region EntityMethods

        public void CreateDataUser(DataUser dataUser)
        {
            context.DataUsers.Add(dataUser);
        }

        //public void UpdateDataUser([Bind(Include = "DataUserId, FirstName, LastName, Email, Address, Number, Complement, District, CEP, BirthDate, Gender, Status, CPF, PJ, Country_CountryId")] DataUser dataUser)
        public void UpdateDataUser(DataUser dataUser)
        {
            context.Entry(dataUser).State = EntityState.Modified;
        }

        public List<DataUser> OtherDataUsers
        {
            get
            {
                long id = WebSecurity.GetUserId(WebSecurity.CurrentUserName);
                return context.DataUsers.Where(d => d.DataUserId != id).ToList();
            }
        }

        public List<DataUser> AllDataUsers
        {
            get
            {
                return context.DataUsers.ToList();
            }
        }

        public string FindUsernameForEmail(string @userLogin)
        {
            DataUser dataUser = context.DataUsers.Where(d => d.Email.Equals(userLogin)).FirstOrDefault();            
            
            if(dataUser != null)
            {
                SimpleMembershipProvider provider = (SimpleMembershipProvider)Membership.Provider;
                string username = provider.GetUserNameFromId((int)dataUser.DataUserId);
                return username;
            }

            return userLogin;
        }

        public DataUser FindDataUserForUserId(long id)
        {
            return context.DataUsers.Find(id);
        }

        public DataUser MyDataUser()
        {
            long id = WebSecurity.GetUserId(WebSecurity.CurrentUserName);
            return context.DataUsers.Find(id);
        }

        public void Delete(long id)
        {
            DataUser dataUser = FindDataUserForUserId(id);
            context.DataUsers.Remove(dataUser);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        #endregion EntityMethods

        #region Dispose

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Dispose
    }
}