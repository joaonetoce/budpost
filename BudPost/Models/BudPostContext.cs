﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BudPost.Areas.Admin.Models;

namespace BudPost.Models
{
    public class BudPostContext : DbContext
    {

        #region TableMap

        public DbSet<DataUser> DataUsers { get; set; }
        
        public DbSet<Phone> Phones { get; set; }

        public DbSet<Poster> Posters { get; set; }

        //Admin
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }

        public DbSet<City> Cities { get; set; }

        #endregion TableMap

        #region BudPostContext

        public BudPostContext()
        {            
            //Sem definição
            DropCreateDatabaseIfModelChanges<BudPostContext> initializer = new DropCreateDatabaseIfModelChanges<BudPostContext>();
            Database.SetInitializer<BudPostContext>(initializer); 
             

            /*
            //Sobreecrita do método Seed para as tabelas Countries, States, Cities (UNIKEY)
            CustomDBInitializer initializer = new CustomDBInitializer();
            Database.SetInitializer<BudPostContext>(initializer); 
             */
        }

        #endregion BudPostContext
    }
}