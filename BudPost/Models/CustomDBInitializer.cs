﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class CustomDBInitializer : DropCreateDatabaseIfModelChanges <BudPostContext>
    {
        protected override void Seed(BudPostContext context)
        {
            context.Database.ExecuteSqlCommand("ALTER TABLE DataUser ADD CONSTRAINT Country UNIQUE(Country_CountryId)");
            context.Database.ExecuteSqlCommand("ALTER TABLE DataUser ADD CONSTRAINT State UNIQUE(State_StateId)");
            context.Database.ExecuteSqlCommand("ALTER TABLE DataUser ADD CONSTRAINT City UNIQUE(City_CityId)");
            base.Seed(context);
        }
    }
}