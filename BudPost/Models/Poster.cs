﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BudPost.Models
{
    public class Poster
    {
        [Key]
        public long PosterId { get; set; }

        [DataType(DataType.Text)]        
        [Required(ErrorMessage = "Você precisa escrever um título!")]
        public string Title { get; set; }

        [DataType(DataType.Text)]        
        [Required(ErrorMessage = "Você precisa descrever o serviço!")]
        public string Description { get; set; }

        public long DataUser_DataUserId { get; set; }

        [ForeignKey("DataUser_DataUserId")]
        public virtual DataUser DataUser { get; set; }
    }
}